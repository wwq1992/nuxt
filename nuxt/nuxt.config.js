module.exports = {
	/*
	 ** Headers of the page
	 */
	head: {
		title: 'nuxt',
		meta: [{
				charset: 'utf-8'
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: 'Nuxt.js project'
			}
		],
		link: [{
			rel: 'icon',
			type: 'image/x-icon',
			href: '/favicon.ico'
		}],
	},
	plugins: [{
			src: '~/plugins/swiper.js',
			ssr: false
		},
		{
			src: '~/plugins/i18n.js'
		},
	],
	css: [
		'~static/base.css',
		'swiper/dist/css/swiper.css'
	],

	router: {
		middleware: 'i18n'
	},
	generate: {
		routes: ['/', '/about', '/privacy', '/terms', '/storage', '/kr', '/kr/about', '/kr/privacy', '/kr/terms', '/en', '/en/about', '/en/privacy', '/en/terms']
	},
	/*
	 ** Customize the progress bar color
	 */
	loading: {
		/*设置进度条样式*/
		color: '#f8b833',
		failedColor: 'red'
	},
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** Run ESLint on save
		 */
		vendor: ['axios', 'lodash'],
		extend(config, {
			isDev,
			isClient
		}) {
			if(isDev && isClient) {
				config.module.rules.push({
					enforce: 'pre',
					test: /\.(js|vue)$/,
					//loader: 'eslint-loader',
					exclude: /(node_modules)/
				})
			}
		}
	},
	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/proxy'
	],
	axios: {
		proxy: true
		// 这里的true也可以是一个包含默认配置的对象
	},
	proxy: {
		'/api': {
			target: 'https://banner-storage-ms.juejin.im',
			//target: 'http://liyabins.mynatapp.cc',
			pathRewrite: {
				'^/api': '/'
			}
		}
	}
}